const express 		= require('express');
const MongoClient 	= require('mongodb').MongoClient;
const app 			= express();
const routes		= require('../routes/counterparty_route');
const config 		= require('../config/api-configuration');
const bodyParser	= require('body-parser')
app.use(bodyParser.urlencoded({extended : true}))

//Creating counter party API server
const counterPartyDbEndPoint = config.counterParty.dbEndPoint;
MongoClient.connect(counterPartyDbEndPoint, (err, database) => {
	console.log(`Starting Counter party API server ...`);
	if(err) {
		console.log('There is a problem connecting to Database', err)
	} else {
		const port = config.counterParty.port;
		console.log(`Connected to database ${counterPartyDbEndPoint}`);
		routes(app, database);
		app.listen(port, () => {
		console.log(`Counter party API server is live on ${port}`);
		})
	}
})

const express 		= require('express');
const MongoClient 	= require('mongodb').MongoClient;
const app 			= express();
const routes		= require('../routes/commodity_route');
const config 		= require('../config/api-configuration');
const bodyParser	= require('body-parser')
app.use(bodyParser.urlencoded({extended : true}))

//Creating commodity API server
const commodityDbEndPoint = config.commodity.dbEndPoint;
MongoClient.connect(commodityDbEndPoint, (err, database) => {
	console.log(`Starting Commodity API server ...`);
	if(err) {
		console.log('There is a problem connecting to Database', err)
	} else {
		const port = config.commodity.port;
		console.log(`Connected to database ${commodityDbEndPoint}`);
		routes(app, database);
		app.listen(port, () => {
		console.log(`COmmodity API server is live on ${port}`);
		})
	}
})
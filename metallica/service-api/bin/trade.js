const express 		= require('express');
const MongoClient 	= require('mongodb').MongoClient;
const app 			= express();
const routes		= require('../routes/trade_route');
const config 		= require('../config/api-configuration');
const bodyParser	= require('body-parser')
app.use(bodyParser.urlencoded({extended : true}))

//Creating trade API server
const tradeDbEndPoint = config.trade.dbEndPoint;
MongoClient.connect(tradeDbEndPoint, (err, database) => {
	console.log(`Starting Trade API server ...`);
	if(err) {
		console.log('There is a problem connecting to Database', err)
	} else {
		const port = config.trade.port;
		console.log(`Connected to database ${tradeDbEndPoint}`);
		routes(app, database);
		app.listen(port, () => {
		console.log(`Trade API server is live on ${port}`);
		})
	}
})
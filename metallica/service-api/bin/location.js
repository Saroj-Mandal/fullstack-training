const express 		= require('express');
const MongoClient 	= require('mongodb').MongoClient;
const app 			= express();
const routes		= require('../routes/location_route');
const config 		= require('../config/api-configuration');
const bodyParser	= require('body-parser')
app.use(bodyParser.urlencoded({extended : true}))

//Creating location API server
const locationDbEndPoint = config.location.dbEndPoint;
MongoClient.connect(locationDbEndPoint, (err, database) => {
	console.log(`Starting Location API server ...`);
	if(err) {
		console.log('There is a problem connecting to Database', err)
	} else {
		const port = config.location.port;
		console.log(`Connected to database ${locationDbEndPoint}`);
		routes(app, database);
		app.listen(port, () => {
		console.log(`Location API server is live on ${port}`);
		})
	}
})

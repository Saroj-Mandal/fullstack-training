const express = require('express');
const proxyRoute = require('../routes/proxy_route');

var app = express();

//calling proxy to register
proxyRoute(app);

//Starting proxy server
app.listen(8010, () => {
    console.log('Proxy server is live in port : 8010');
});
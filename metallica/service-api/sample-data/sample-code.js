var greeting = (name) => {
    console.log("Hello " + name);
}

greeting("Saroj");

const http = require('http');

const hostName = "127.0.0.1";
const port = "4040";

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('hello World');
});

server.listen(port, hostName, () => {
    console.log(`Server live on Host: ${hostName} and Port: ${port}`);
});
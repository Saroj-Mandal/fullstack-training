const util 		= require('util')
var ObjectID 	= require('mongodb').ObjectID

module.exports = (app, db) => {

	//Create counterParty API
	app.post('/counterParties', (req, res) => {
		const logApiMessage = 'Inside create counterParty API : ';
		//We will create new counterParty here.
		console.log(logApiMessage + util.inspect(req.body, false, null));
		const counterPartyData = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('counterParties').insert(counterPartyData, (err, result) => {
			if(err){
				console.log(logApiMessage + `Couldn't create counterParty`, err);
				res.send('Couldn\'t create counterParty');
			} else {
				console.log(logApiMessage + `counterParty created ${util.inspect(result.ops[0], {'showHidden' : false, 'depth' : null})}`);
				res.send(result.ops[0]);
			}
		});
	});

	//Get all counterParty API
	app.get('/counterParties', (req, res) => {
		const logApiMessage = 'Inside get All counterParty API : ';
		//we will get all counterParty from our database
		console.log(logApiMessage + util.inspect(req.body, false, null));
		db.collection('counterParties').find({}).toArray((err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find counterParty.');
			} else if(result.length) {
				console.log(logApiMessage + `result returned ${util.inspect(result, false, null)}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Get counterParty API
	app.get('/counterParties/:counterPartyId', (req, res) => {
		const logApiMessage = 'Inside get counterParty API : ';
		//we will get particular counterParty from our database
		let counterPartyId = req.params.counterPartyId;
		console.log(logApiMessage + `fetching counterParty with id ${counterPartyId}`);
		const counterParty = {'_id' : new ObjectID(counterPartyId)};
		db.collection('counterParties').findOne(counterParty, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find counterParty.');
			} else if(true) {
				console.log(logApiMessage + `counterParty found with id ${counterPartyId}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Update counterParty API
	app.put('/counterParties/:counterPartyId', (req, res) => {
		const logApiMessage = 'Inside update counterParty API : ';
		//we will update counterParty in our database
		const counterPartyId = req.params.counterPartyId;
		console.log(logApiMessage + `updating counterParty with id ${counterPartyId}`);
		const counterPartyToUpdate = {'_id' : new ObjectID(counterPartyId)};
		const updatedValue = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('counterParties').update(counterPartyToUpdate, updatedValue, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update counterParty.');
			} else {
				console.log(logApiMessage + `counterParty updated with id ${counterPartyId}`);
				res.send(result);
			}
		});
	});

	//Delete counterParty API
	app.delete('/counterParties/:counterPartyId', (req, res) => {
		const logApiMessage = 'Inside delete counterParty API : ';
		//we will delete counterParty from our database
		const counterPartyId = req.params.counterPartyId;
		console.log(logApiMessage + `deleting counterParty with id ${counterPartyId}`);
		const counterPartyToDelete = {'_id' : new ObjectID(counterPartyId)};
		db.collection('counterParties').remove(counterPartyToDelete, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update counterParty.');
			} else {
				console.log(logApiMessage + `counterParty with id ${counterPartyId} is deleted`);
				res.send(result);
			}
		});
	});
}
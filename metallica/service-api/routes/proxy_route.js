const express 		    = require('express');
const proxy             = require('http-proxy-middleware');
const apiConfig 		= require('../config/api-configuration');
const commonConfig 		= require('../config/common-configuration');

const domain = commonConfig.app.domain;
const scheme = commonConfig.app.scheme;
const proxyPort = commonConfig.proxy.port;

const proxyUrl = scheme + '://' + domain + ':' + proxyPort;
const locationApiUrl = scheme + '://' + domain + ':' + apiConfig.location.port;
const counterPartyApiUrl = scheme + '://' + domain + ':' + apiConfig.counterParty.port;
const commodityApiUrl = scheme + '://' + domain + ':' + apiConfig.commodity.port;
const tradeApiUrl = scheme + '://' + domain + ':' + apiConfig.trade.port;

const locationActualPath = apiConfig.location.actualPath;

const counterPartyActualPath = apiConfig.counterParty.actualPath;

const commodityActualPath = apiConfig.commodity.actualPath;

const tradeActualPath = apiConfig.trade.actualPath;

//proxy middleware options for location API
var locationProxyOptions = {
    target : locationApiUrl,                        //target host
    changeOrigin : true,                            //needed for virtual hosted site
    ws : true,                                      //proxy web socket
    pathRewrite : {
        '^/api/locations' : locationActualPath     //rewriting path
    },
    router : {
        proxyUrl : locationApiUrl                   //re-routing to actual URL from proxy server
    }
}

//proxy middleware options for counter party API
var counterPartyProxyOptions = {
    target : counterPartyApiUrl,                    //target host
    changeOrigin : true,                            //needed for virtual hosted site
    ws : true,                                      //proxy web socket
    pathRewrite : {
        '^/api/counterparties' : counterPartyActualPath //rewriting path
    },
    router : {
        proxyUrl : counterPartyApiUrl                 //re-routing to actual URL from proxy server
    }
}

//proxy middleware options for commodity API
var commodityProxyOptions = {
    target : commodityApiUrl,                       //target host
    changeOrigin : true,                            //needed for virtual hosted site
    ws : true,                                      //proxy web socket
    pathRewrite : {
        '^/api/commodities' : commodityActualPath     //rewriting path
    },
    router : {
        proxyUrl : commodityApiUrl                    //re-routing to actual URL from proxy server
    }
}

//proxy middleware options for trade API
var tradeProxyOptions = {
    target : tradeApiUrl,                           //target host
    changeOrigin : true,                            //needed for virtual hosted site
    ws : true,                                      //proxy web socket
    pathRewrite : {
        '^/api/trades' : tradeActualPath             //rewriting path
    },
    router : {
        proxyUrl : tradeApiUrl                        //re-routing to actual URL from proxy server
    }
}

module.exports = (app) => {
//mount proxy in  the web server
console.log('registering proxy....');
app.use('/api/locations', proxy(locationProxyOptions));
app.use('/api/counterparties', proxy(counterPartyProxyOptions));
app.use('/api/commodities', proxy(commodityProxyOptions));
app.use('/api/trades', proxy(tradeProxyOptions));
}
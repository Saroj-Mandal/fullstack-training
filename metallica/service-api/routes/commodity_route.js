const util 		= require('util')
var ObjectID 	= require('mongodb').ObjectID

module.exports = (app, db) => {

	//Create commodity API
	app.post('/commodities', (req, res) => {
		const logApiMessage = 'Inside create commodity API : ';
		//We will create new commodity here.
		console.log(logApiMessage + util.inspect(req.body, false, null));
		const commodityData = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('commodities').insert(commodityData, (err, result) => {
			if(err){
				console.log(logApiMessage + `Couldn't create commodity`, err);
				res.send('Couldn\'t create commodity');
			} else {
				console.log(logApiMessage + `commodity created ${util.inspect(result.ops[0], {'showHidden' : false, 'depth' : null})}`);
				res.send(result.ops[0]);
			}
		});
	});

	//Get all commodity API
	app.get('/commodities', (req, res) => {
		const logApiMessage = 'Inside get All commodity API : ';
		//we will get all commodity from our database
		console.log(logApiMessage + util.inspect(req.body, false, null));
		db.collection('commodities').find({}).toArray((err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find commodity.');
			} else if(result.length) {
				console.log(logApiMessage + `result returned ${util.inspect(result, false, null)}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Get commodity API
	app.get('/commodities/:commodityId', (req, res) => {
		const logApiMessage = 'Inside get commodity API : ';
		//we will get particular commodity from our database
		let commodityId = req.params.commodityId;
		console.log(logApiMessage + `fetching commodity with id ${commodityId}`);
		const commodity = {'_id' : new ObjectID(commodityId)};
		db.collection('commodities').findOne(commodity, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find commodity.');
			} else if(true) {
				console.log(logApiMessage + `commodity found with id ${commodityId}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Update commodity API
	app.put('/commodities/:commodityId', (req, res) => {
		const logApiMessage = 'Inside update commodity API : ';
		//we will update commodity in our database
		const commodityId = req.params.commodityId;
		console.log(logApiMessage + `updating commodity with id ${commodityId}`);
		const commodityToUpdate = {'_id' : new ObjectID(commodityId)};
		const updatedValue = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('commodities').update(commodityToUpdate, updatedValue, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update commodity.');
			} else {
				console.log(logApiMessage + `commodity updated with id ${commodityId}`);
				res.send(`commodity updated with id ${commodityId}`);
			}
		});
	});

	//Delete commodity API
	app.delete('/commodities/:commodityId', (req, res) => {
		const logApiMessage = 'Inside delete commodity API : ';
		//we will delete commodity from our database
		const commodityId = req.params.commodityId;
		console.log(logApiMessage + `deleting commodity with id ${commodityId}`);
		const commodityToDelete = {'_id' : new ObjectID(commodityId)};
		db.collection('commodities').remove(commodityToDelete, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update commodity.');
			} else {
				console.log(logApiMessage + `commodity with id ${commodityId} is deleted`);
				res.send(`commodity with id ${commodityId} is deleted`);
			}
		});
	});
}
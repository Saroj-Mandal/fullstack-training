const util 		= require('util')
var ObjectID 	= require('mongodb').ObjectID

module.exports = (app, db) => {

	//Create trade API
	app.post('/trades', (req, res) => {
		const logApiMessage = 'Inside create trade API : ';
		//We will create new trade here.
		console.log(logApiMessage + util.inspect(req.body, false, null));
		const tradeData = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('trades').insert(tradeData, (err, result) => {
			if(err){
				console.log(logApiMessage + `Couldn't create trade`, err);
				res.send('Couldn\'t create trade');
			} else {
				console.log(logApiMessage + `trade created ${util.inspect(result.ops[0], {'showHidden' : false, 'depth' : null})}`);
				res.send(result.ops[0]);
			}
		});
		//db.close();
	});

	//Get all trade API
	app.get('/trades', (req, res) => {
		const logApiMessage = 'Inside get All trade API : ';
		//we will get all trade from our database
		console.log(logApiMessage + util.inspect(req.body, false, null));
		db.collection('trades').find({}).toArray((err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find trade.');
			} else if(result.length) {
				console.log(logApiMessage + `result returned ${util.inspect(result, false, null)}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Get trade API
	app.get('/trades/:tradeId', (req, res) => {
		const logApiMessage = 'Inside get trade API : ';
		//we will get particular trade from our database
		let tradeId = req.params.tradeId;
		console.log(logApiMessage + `fetching trade with id ${tradeId}`);
		const trade = {'_id' : new ObjectID(tradeId)};
		db.collection('trades').findOne(trade, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find trade.');
			} else if(true) {
				console.log(logApiMessage + `trade found with id ${tradeId}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Update trade API
	app.put('/trades/:tradeId', (req, res) => {
		const logApiMessage = 'Inside update trade API : ';
		//we will update trade in our database
		const tradeId = req.params.tradeId;
		console.log(logApiMessage + `updating trade with id ${tradeId}`);
		const tradeToUpdate = {'_id' : new ObjectID(tradeId)};
		const updatedValue = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('trades').update(tradeToUpdate, updatedValue, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update trade.');
			} else {
				console.log(logApiMessage + `trade updated with id ${tradeId}`);
				res.send(result);
			}
		});
	});

	//Delete trade API
	app.delete('/trades/:tradeId', (req, res) => {
		const logApiMessage = 'Inside delete trade API : ';
		//we will delete trade from our database
		const tradeId = req.params.tradeId;
		console.log(logApiMessage + `deleting trade with id ${tradeId}`);
		const tradeToDelete = {'_id' : new ObjectID(tradeId)};
		db.collection('trades').remove(tradeToDelete, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update trade.');
			} else {
				console.log(logApiMessage + `trade with id ${tradeId} is deleted`);
				res.send(result);
			}
		});
	});
}
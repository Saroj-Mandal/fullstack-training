const util 		= require('util')
var ObjectID 	= require('mongodb').ObjectID

module.exports = (app, db) => {

	//Create location API
	app.post('/locations', (req, res) => {
		const logApiMessage = 'Inside create location API : ';
		//We will create new location here.
		console.log(logApiMessage + util.inspect(req.body, false, null));
		const locationData = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('locations').insert(locationData, (err, result) => {
			if(err){
				console.log(logApiMessage + `Couldn't create location`, err);
				res.send('Couldn\'t create location');
			} else {
				console.log(logApiMessage + `Location created ${util.inspect(result.ops[0], {'showHidden' : false, 'depth' : null})}`);
				res.send(result.ops[0]);
			}
		});
		//db.close();
	});

	//Get all location API
	app.get('/locations', (req, res) => {
		const logApiMessage = 'Inside get All location API : ';
		//we will get all location from our database
		console.log(logApiMessage + util.inspect(req.body, false, null));
		db.collection('locations').find({}).toArray((err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find location.');
			} else if(result.length) {
				console.log(logApiMessage + `result returned ${util.inspect(result, false, null)}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Get location API
	app.get('/locations/:locationId', (req, res) => {
		const logApiMessage = 'Inside get location API : ';
		//we will get particular location from our database
		let locationId = req.params.locationId;
		console.log(logApiMessage + `fetching location with id ${locationId}`);
		const location = {'_id' : new ObjectID(locationId)};
		db.collection('locations').findOne(location, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to find location.');
			} else if(true) {
				console.log(logApiMessage + `location found with id ${locationId}`);
				res.send(result);
			} else {
				console.log(logApiMessage + 'No data found');
				res.send('No data found');
			}
		});
	});

	//Update location API
	app.put('/locations/:locationId', (req, res) => {
		const logApiMessage = 'Inside update location API : ';
		//we will update location in our database
		const locationId = req.params.locationId;
		console.log(logApiMessage + `updating location with id ${locationId}`);
		const locationToUpdate = {'_id' : new ObjectID(locationId)};
		const updatedValue = {'code' : req.body.code, 'description' : req.body.name};
		db.collection('locations').update(locationToUpdate, updatedValue, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update location.');
			} else {
				console.log(logApiMessage + `location updated with id ${locationId}`);
				res.send(result);
			}
		});
	});

	//Delete location API
	app.delete('/locations/:locationId', (req, res) => {
		const logApiMessage = 'Inside delete location API : ';
		//we will delete location from our database
		const locationId = req.params.locationId;
		console.log(logApiMessage + `deleting location with id ${locationId}`);
		const locationToDelete = {'_id' : new ObjectID(locationId)};
		db.collection('locations').remove(locationToDelete, (err, result) => {
			if(err) {
				console.log(logApiMessage, err);
				res.send('Unable to update location.');
			} else {
				console.log(logApiMessage + `location with id ${locationId} is deleted`);
				res.send(result);
			}
		});
	});
}